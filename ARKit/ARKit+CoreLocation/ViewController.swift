//
//  ViewController.swift
//  ARKitTestAPP
//
//  Created by Nikita Novikov on 12/11/2017.
//  Copyright © 2017 Project Dent. All rights reserved.
//

import UIKit
import SceneKit 
import MapKit
import CocoaLumberjack

@available(iOS 11.0, *)
class ViewController: UIViewController, MKMapViewDelegate, SceneLocationViewDelegate {
    let sceneLocationView = SceneLocationView()
	
    let mapView = MKMapView()
    var userAnnotation: MKPointAnnotation?
    var locationEstimateAnnotation: MKPointAnnotation?
    var updateUserLocationTimer: Timer?
	var showMapView: Bool = true
    var centerMapOnUserLocation: Bool = true
    var displayDebugging = false
    var infoLabel = UILabel()
    var updateInfoLabelTimer: Timer?
    var adjustNorthByTappingSidesOfScreen = false
    
    override func viewDidLoad() {
	
        super.viewDidLoad()
        
        infoLabel.font = UIFont.systemFont(ofSize: 10)
        infoLabel.textAlignment = .left
        infoLabel.textColor = UIColor.white
        infoLabel.numberOfLines = 0
        sceneLocationView.addSubview(infoLabel)
        
        updateInfoLabelTimer = Timer.scheduledTimer(
            timeInterval: 0.1,
            target: self,
            selector: #selector(ViewController.updateInfoLabel),
            userInfo: nil,
            repeats: true)
		
        sceneLocationView.showAxesNode = false
        sceneLocationView.locationDelegate = self
        
		addLocations()
		
		view.addSubview(sceneLocationView)
		
		if showMapView {
			mapView.delegate = self
			mapView.showsUserLocation = true
			view.addSubview(mapView)
			
			updateUserLocationTimer = Timer.scheduledTimer(
				timeInterval: 0.5,
				target: self,
				selector: #selector(ViewController.updateUserLocation),
				userInfo: nil,
				repeats: true)
		}
    }
	
	func addLocations() {
		//добавление картинок и локаций на карту
		//Добавляю компания KR
		let pinImageKR = UIImage(named: "kr")!
		let pinCoordinateKR = CLLocationCoordinate2D(latitude: 55.744195, longitude: 37.560389)
		let pinLocationKR = CLLocation(coordinate: pinCoordinateKR, altitude: 236)
		let pinLocationNodeKR = LocationAnnotationNode(location: pinLocationKR, image: pinImageKR)
		sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: pinLocationNodeKR)
		
		let annotationKR = MKPointAnnotation()
		annotationKR.coordinate = pinCoordinateKR
		annotationKR.title = "KR"
		mapView.addAnnotation(annotationKR)
		
		//Добавляю компания красную площадь
		let pinImageRedSquare = UIImage(named: "red-square")!
		let pinCoordinateRedSquare = CLLocationCoordinate2D(latitude:55.753921, longitude: 37.620525)
		let pinLocationRedSquare = CLLocation(coordinate: pinCoordinateRedSquare, altitude: 236)
		let pinLocationNodeRedSquare = LocationAnnotationNode(location: pinLocationRedSquare, image: pinImageRedSquare)
		sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: pinLocationNodeRedSquare)
		
		let annotationRedSquare = MKPointAnnotation()
		annotationRedSquare.coordinate = pinCoordinateRedSquare
		annotationRedSquare.title = "Красная площадь"
		mapView.addAnnotation(annotationRedSquare)
		
		//Добавляю ТЦ Европейский
		let pinImageEuropeanSC = UIImage(named: "europe")!
		let pinCoordinateEuropeanSC = CLLocationCoordinate2D(latitude:55.744657, longitude:37.565925)
		let pinLocationEuropeanSC = CLLocation(coordinate: pinCoordinateEuropeanSC, altitude: 236)
		let pinLocationNodeEuropeanSC = LocationAnnotationNode(location: pinLocationEuropeanSC, image: pinImageEuropeanSC)
		sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: pinLocationNodeEuropeanSC)
		
		let annotationEuropeanSC = MKPointAnnotation()
		annotationEuropeanSC.coordinate = pinCoordinateEuropeanSC
		annotationEuropeanSC.title = "ТЦ Европейский"
		mapView.addAnnotation(annotationEuropeanSC)
		
		
		//добавление прямых
		// координаты здания KR, красный цвет
		let pinImage = UIImage(named: "pin")!
		let pinCoordinate1 = CLLocationCoordinate2D(latitude: 55.744123, longitude: 37.559903)
		let pinLocation1 = CLLocation(coordinate: pinCoordinate1, altitude: 50)
		let pinLocationNode1 = LocationAnnotationNode(location: pinLocation1, image: pinImage)
		
		let locationNodeLocation1 = pinLocationNode1.location!
		
		let locationNodeLocation1Translation = SCNVector3(
			x: Float(pinLocation1.coordinate.longitude),
			y: Float(pinLocation1.altitude),
			z: Float(pinLocation1.coordinate.latitude))
		
		let pinCoordinate2 = CLLocationCoordinate2D(latitude: 55.744235, longitude: 37.561051)
		let pinLocation2 = CLLocation(coordinate: pinCoordinate2, altitude: 150)
		let pinLocationNode2 = LocationAnnotationNode(location: pinLocation2, image: pinImage)
		
		
		let locationNodeLocation2Translation = SCNVector3(
			x: Float(pinLocation2.coordinate.longitude),
			y: Float(pinLocation2.altitude),
			z: Float(pinLocation2.coordinate.latitude))
		sceneLocationView.addVectorLocationNodeWithConfirmedLocation(locationNodeFrom: pinLocationNode1, locationNodeTo: pinLocationNode2)
		
		// координаты диагонали тц Европейсикй, белый цвет
		let pinCoordinate3 = CLLocationCoordinate2D(latitude: 55.743193, longitude: 37.564278)
		let pinLocation3 = CLLocation(coordinate: pinCoordinate3, altitude: 120)
		let pinLocationNode3 = LocationAnnotationNode(location: pinLocation3, image: pinImage)
		
		let locationNodeLocation3 = pinLocationNode3.location!
		
		let locationNodeLocation3Translation = SCNVector3(
			x: Float(pinLocation3.coordinate.longitude),
			y: Float(pinLocation3.altitude),
			z: Float(pinLocation3.coordinate.latitude))
		
		let pinCoordinate4 = CLLocationCoordinate2D(latitude: 55.745566, longitude: 37.566917)
		let pinLocation4 = CLLocation(coordinate: pinCoordinate4, altitude: 120)
		let pinLocationNode4 = LocationAnnotationNode(location: pinLocation4, image: pinImage)
		
		let locationNodeLocation4Translation = SCNVector3(
			x: Float(pinLocation4.coordinate.longitude),
			y: Float(pinLocation4.altitude),
			z: Float(pinLocation4.coordinate.latitude))
		sceneLocationView.addVectorLocationNodeWithConfirmedLocation(locationNodeFrom: pinLocationNode3, locationNodeTo: pinLocationNode4)

	}
	
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sceneLocationView.run()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
		sceneLocationView.pause()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        sceneLocationView.frame = CGRect(
            x: 0,
            y: 0,
            width: self.view.frame.size.width,
            height: self.view.frame.size.height)
        
        infoLabel.frame = CGRect(x: 6, y: 0, width: self.view.frame.size.width - 12, height: 14 * 4)
        
        if showMapView {
            infoLabel.frame.origin.y = (self.view.frame.size.height * (2/3)) - infoLabel.frame.size.height
        } else {
            infoLabel.frame.origin.y = self.view.frame.size.height - infoLabel.frame.size.height
        }
        
        mapView.frame = CGRect(
            x: 0,
            y: self.view.frame.size.height * (2/3),
            width: self.view.frame.size.width,
            height: self.view.frame.size.height / 3)
	}
    
    @objc func updateUserLocation() {
        if let currentLocation = sceneLocationView.currentLocation() {
            DispatchQueue.main.async {

                if self.userAnnotation == nil {
                    self.userAnnotation = MKPointAnnotation()
                    self.mapView.addAnnotation(self.userAnnotation!)
                }
                
                UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.allowUserInteraction, animations: {
                    self.userAnnotation?.coordinate = currentLocation.coordinate
                }, completion: nil)
            
                if self.centerMapOnUserLocation {
                    UIView.animate(withDuration: 0.45, delay: 0, options: UIViewAnimationOptions.allowUserInteraction, animations: {
                        self.mapView.setCenter(self.userAnnotation!.coordinate, animated: false)
                    }, completion: {
                        _ in
                        self.mapView.region.span = MKCoordinateSpan(latitudeDelta: 0.0005, longitudeDelta: 0.0005)
                    })
                }
                
                if self.displayDebugging {
                    let bestLocationEstimate = self.sceneLocationView.bestLocationEstimate()
                    
                    if bestLocationEstimate != nil {
                        if self.locationEstimateAnnotation == nil {
                            self.locationEstimateAnnotation = MKPointAnnotation()
                            self.mapView.addAnnotation(self.locationEstimateAnnotation!)
                        }
                        
                        self.locationEstimateAnnotation!.coordinate = bestLocationEstimate!.location.coordinate
                    } else {
                        if self.locationEstimateAnnotation != nil {
                            self.mapView.removeAnnotation(self.locationEstimateAnnotation!)
                            self.locationEstimateAnnotation = nil
                        }
                    }
                }
            }
        }
    }
    
    @objc func updateInfoLabel() {
        if let position = sceneLocationView.currentScenePosition() {
            infoLabel.text = "x: \(String(format: "%.2f", position.x)), y: \(String(format: "%.2f", position.y)), z: \(String(format: "%.2f", position.z))\n"
        }
        
        if let eulerAngles = sceneLocationView.currentEulerAngles() {
            infoLabel.text!.append("Euler x: \(String(format: "%.2f", eulerAngles.x)), y: \(String(format: "%.2f", eulerAngles.y)), z: \(String(format: "%.2f", eulerAngles.z))\n")
        }
        
        if let heading = sceneLocationView.locationManager.heading,
            let accuracy = sceneLocationView.locationManager.headingAccuracy {
            infoLabel.text!.append("Heading: \(heading)º, accuracy: \(Int(round(accuracy)))º\n")
        }
        
        let date = Date()
        let comp = Calendar.current.dateComponents([.hour, .minute, .second, .nanosecond], from: date)
        
        if let hour = comp.hour, let minute = comp.minute, let second = comp.second, let nanosecond = comp.nanosecond {
            infoLabel.text!.append("\(String(format: "%02d", hour)):\(String(format: "%02d", minute)):\(String(format: "%02d", second)):\(String(format: "%03d", nanosecond / 1000000))")
        }
    }
	
    
    //MARK: MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        if let pointAnnotation = annotation as? MKPointAnnotation {
            let marker = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: nil)
            
            if pointAnnotation == self.userAnnotation {
                marker.displayPriority = .required
                marker.glyphImage = UIImage(named: "user")
            } else {
                marker.displayPriority = .required
                marker.markerTintColor = UIColor(hue: 0.267, saturation: 0.67, brightness: 0.77, alpha: 1.0)
                marker.glyphImage = UIImage(named: "compass")
            }
            
            return marker
        }
        
        return nil
    }
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesBegan(touches, with: event)
		
		if let touch = touches.first {
			if touch.view != nil {
				if (mapView == touch.view! ||
					mapView.recursiveSubviews().contains(touch.view!)) {
					centerMapOnUserLocation = false
				} else {
					
					let location = touch.location(in: self.view)
					
					if location.x <= 40 && adjustNorthByTappingSidesOfScreen {
						print("left side of the screen")
						sceneLocationView.moveSceneHeadingAntiClockwise()
					} else if location.x >= view.frame.size.width - 40 && adjustNorthByTappingSidesOfScreen {
						print("right side of the screen")
						sceneLocationView.moveSceneHeadingClockwise()
					}
				}
			}
		}
	}
    //MARK: SceneLocationViewDelegate
    
    func sceneLocationViewDidAddSceneLocationEstimate(sceneLocationView: SceneLocationView, position: SCNVector3, location: CLLocation) {
        DDLogDebug("add scene location estimate, position: \(position), location: \(location.coordinate), accuracy: \(location.horizontalAccuracy), date: \(location.timestamp)")
    }
    
    func sceneLocationViewDidRemoveSceneLocationEstimate(sceneLocationView: SceneLocationView, position: SCNVector3, location: CLLocation) {
        DDLogDebug("remove scene location estimate, position: \(position), location: \(location.coordinate), accuracy: \(location.horizontalAccuracy), date: \(location.timestamp)")
    }
    
    func sceneLocationViewDidConfirmLocationOfNode(sceneLocationView: SceneLocationView, node: LocationNode) {
    }
    
    func sceneLocationViewDidSetupSceneNode(sceneLocationView: SceneLocationView, sceneNode: SCNNode) {
        
    }
    
    func sceneLocationViewDidUpdateLocationAndScaleOfLocationNode(sceneLocationView: SceneLocationView, locationNode: LocationNode) {
        
    }
}

extension DispatchQueue {
    func asyncAfter(timeInterval: TimeInterval, execute: @escaping () -> Void) {
        self.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(timeInterval * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: execute)
    }
}

extension UIView {
    func recursiveSubviews() -> [UIView] {
        var recursiveSubviews = self.subviews
        
        for subview in subviews {
            recursiveSubviews.append(contentsOf: subview.recursiveSubviews())
        }
        
        return recursiveSubviews
    }
}
