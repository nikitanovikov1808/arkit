//
//  FloatingPoint+Radians.swift
//  ARKitTestAPP
//
//  Created by Nikita Novikov on 12/11/2017.
//  Copyright © 2017 Project Dent. All rights reserved.
//

import Foundation

public extension FloatingPoint {
    public var degreesToRadians: Self { return self * .pi / 180 }
    public var radiansToDegrees: Self { return self * 180 / .pi }
}
